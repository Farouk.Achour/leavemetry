/* eslint-disable @typescript-eslint/no-explicit-any */
import { createAsyncThunk } from '@reduxjs/toolkit'
import { ForgotPasswordPayload, LoginPayload, ResetPasswordPayload } from './authTypes'
import supabase from '@src/modules/shared/services/supabase'
import { clearTokens, setTokens } from '../utils/token'

export const login = createAsyncThunk(
  'auth/login',
  async (query: LoginPayload, { rejectWithValue }) => {
    try {
      let { data, error }: any = await supabase.auth.signInWithPassword(query)

      if (error) throw new Error(error.message)

      setTokens(data.session?.access_token, data.session?.refresh_token)

      return data
    } catch (err: any) {
      return rejectWithValue(err)
    }
  }
)

export const updatePassword = createAsyncThunk(
  'auth/set-password',
  async (query: LoginPayload, { rejectWithValue }) => {
    try {
      const {
        data: { users },
        error,
      } = await supabase.auth.admin.listUsers()

      if (error) throw new Error(error?.message)

      const userId = users.filter((user) => query.email === user.email)[0]?.id

      const { data: user, error: updatingError } = await supabase.auth.admin.updateUserById(
        userId || '',
        { password: query?.password }
      )

      if (updatingError) throw new Error(updatingError?.message)

      return user
    } catch (err: any) {
      return rejectWithValue(err)
    }
  }
)

export const forgotPassword = createAsyncThunk(
  'auth/forgot-password',
  async (query: ForgotPasswordPayload, { rejectWithValue }) => {
    try {
      let { data, error } = await supabase.auth.resetPasswordForEmail(query?.email, {
        redirectTo: `http://localhost:3000/auth/reset-password?email=${query?.email}`,
      })

      if (error) throw new Error(error?.message)

      return data
    } catch (err: any) {
      return rejectWithValue(err)
    }
  }
)

export const resetPassword = createAsyncThunk(
  'auth/reset-password',
  async (query: ResetPasswordPayload, { rejectWithValue }) => {
    try {
      const {
        data: { users },
        error,
      } = await supabase.auth.admin.listUsers()

      if (error) throw new Error(error?.message)

      const userId = users.filter((user) => query.email === user.email)[0]?.id

      const { data: user, error: updatingError } = await supabase.auth.admin.updateUserById(
        userId || '',
        { password: query?.password }
      )

      if (updatingError) throw new Error(updatingError?.message)

      return user
    } catch (err: any) {
      return rejectWithValue(err)
    }
  }
)

export const logout = createAsyncThunk('auth/logout', async (_, { rejectWithValue }) => {
  try {
    let data = await supabase.auth.signOut()

    if (data.error) throw new Error(data.error?.message)

    clearTokens()
    return null
  } catch (err: any) {
    return rejectWithValue(err)
  }
})
