import Button from '@src/modules/shared/components/Button/Button'
import { useAppDispatch } from '@src/modules/shared/store'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { forgotPassword } from '../../data/authThunk'
import Input from '@src/modules/shared/components/Input/Input'
import { getChangedValues } from '@src/modules/shared/utils/getChangedValuesFormik'
import { useState } from 'react'
import LogoIcon from '../../../shared/assets/icons/sidebar/logo.png'
import { toast } from 'react-hot-toast'

const initialValues = {
  username: '',
}

const ForgotPassword = () => {
  const dispatch = useAppDispatch()

  const [submitting, setSubmitting] = useState(false)

  const formik = useFormik({
    initialValues,
    validationSchema: Yup.object().shape({
      email: Yup.string().required('Email is required'),
    }),
    onSubmit: (values) => {
      setSubmitting(true)
      const changedValues = getChangedValues(values, initialValues)

      dispatch(forgotPassword(changedValues))
        .unwrap()
        .then(() => {
          toast.success('A reset link is sent successfully to your email')
        })
        .catch((err) => {
          toast.error(err?.message || 'something-went-wrong')
        })
        .finally(() => {
          setSubmitting(false)
        })
    },
  })

  return (
    <div className="login-module">
      <img
        src="http://localhost:3000/src/modules/shared/assets/images/auth/auth-img.png"
        className="auth-img"
      />

      <div className="auth-container">
        <img src={LogoIcon} className="logo" alt="" />

        <form className="login-card-container" onSubmit={formik.handleSubmit}>
          <h1 className="title">Forgot Password?</h1>

          <p className="description">
            Enter the email address you used when you joined and we'll send you instructions to
            reset your password. For security reasons, we do NOT store your password. So rest
            assured that we will never send your password via email.
          </p>

          <Input
            name="email"
            formik={formik}
            variant="secondary"
            placeholder="Enter your email"
            label="Email Address"
            required={true}
          />

          <Button label={'Send reset instruction'} type={'submit'} loading={submitting} />
        </form>
      </div>
    </div>
  )
}

export default ForgotPassword
