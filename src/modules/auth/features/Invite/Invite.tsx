import Button from '@src/modules/shared/components/Button/Button'
import { useAppDispatch } from '@src/modules/shared/store'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { updatePassword } from '../../data/authThunk'
import Input from '@src/modules/shared/components/Input/Input'
import { getChangedValues } from '@src/modules/shared/utils/getChangedValuesFormik'
import { useState } from 'react'
import { Link, useNavigate, useSearchParams } from 'react-router-dom'
import { PATH } from '../../routes/paths'
import LogoIcon from '../../../shared/assets/icons/sidebar/logo.png'
import { toast } from 'react-hot-toast'

function Invite() {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const [searchParams] = useSearchParams()

  const email = searchParams.get('email')

  const [submitting, setSubmitting] = useState(false)

  const initialValues = {
    email,
    password: '',
  }

  const formik = useFormik({
    initialValues,
    validationSchema: Yup.object().shape({
      email: Yup.string().required('Email is required'),
      password: Yup.string().required('Password is required').min(6, 'Password is too short!'),
    }),
    onSubmit: (values) => {
      setSubmitting(true)
      const changedValues = { ...getChangedValues(values, initialValues), email }

      dispatch(updatePassword(changedValues))
        .unwrap()
        .then(() => {
          navigate('/login')
          toast.success(
            'You accepted the invitation. Now log in to your account using the same info!'
          )
        })
        .catch((err) => {
          toast.error(err?.message || 'something-went-wrong')
        })
        .finally(() => {
          setSubmitting(false)
        })
    },
  })

  return (
    <div className="login-module">
      <img
        src="http://localhost:3000/src/modules/shared/assets/images/auth/auth-img.png"
        className="auth-img"
      />

      <div className="auth-container">
        <img src={LogoIcon} className="logo" alt="" />

        <form className="login-card-container" onSubmit={formik.handleSubmit}>
          <h1 className="title">Accept the invitation</h1>

          <p className="description">Fill up your account Details</p>

          <Input
            name="email"
            formik={formik}
            variant="secondary"
            placeholder="Enter your email"
            label="Email Address"
            required={true}
            disabled={true}
          />

          <Input
            name="password"
            formik={formik}
            variant="secondary"
            placeholder="Enter your password"
            label="Password"
            type="password"
            required={true}
          />

          <Button label={'Accept Invitation'} type={'submit'} loading={submitting} />

          <Link to={PATH.LOGIN} className="link">
            Log In?
          </Link>
        </form>
      </div>
    </div>
  )
}

export default Invite
