import { useMutation } from '@tanstack/react-query'
import { toast } from 'react-hot-toast'
import { deleteUser as deleteUserApi } from '../../shared/services/apiAuth'

export function useDeleteUser() {
  const { mutate: deleteUser } = useMutation({
    mutationFn: deleteUserApi,
    onSuccess: () => {
      // toast.success('User successfully deleted')
    },
    onError: (err) => toast.error(err.message),
  })

  return { deleteUser }
}
