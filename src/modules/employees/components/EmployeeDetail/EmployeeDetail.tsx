import { ReactComponent as InfoIcon } from '../../../shared/assets/icons/Info-circle.svg'
import { ReactComponent as EditIcon } from '../../../shared/assets/icons/edit.svg'
import { ReactComponent as HideIcon } from '../../../shared/assets/icons/cross.svg'
import { IEmployee } from '../EmployeeRow/EmployeeRow'
import useNavigateParams from '@src/modules/shared/hooks/useNavigateParams'

interface IEmployeeDetailProps extends IEmployee {
  handleShowDetails(): void
}

function EmployeeDetail({ employee, handleShowDetails }: IEmployeeDetailProps) {
  const {
    avatar,
    fullName,
    id,
    birthday,
    gender,
    job,
    department,
    status,
    hiringDate,
    email,
    phoneNumber1,
    address,
  } = employee

  const navigateParams = useNavigateParams()

  function handleClickOnDetails() {
    navigateParams('/employees/info', {
      id: id + '',
    })
  }

  function handleClickOnEdit() {
    navigateParams('/employees/edit', {
      id: id + '',
    })
  }

  return (
    <div className="employee-detail">
      <div>
        {avatar ? (
          <img src={avatar} alt="avatar" className="avatar" />
        ) : (
          <div className="avatar avatar-letter">{fullName.slice(0, 1)}</div>
        )}
      </div>
      <div className="employee-detail-grid">
        <div className="employee-detail-list">
          <div className="label">Full Name</div>
          <div>{fullName}</div>
          <div className="label">ID</div>
          <div>{id}</div>
          <div className="label">Birthday</div>
          <div>{birthday}</div>
          <div className="label">Gender</div>
          <div>{gender}</div>
        </div>
        <div className="employee-detail-list">
          <div className="label">Job Title</div>
          <div>{job}</div>
          <div className="label">Department</div>
          <div>{department}</div>
          <div className="label">Status</div>
          <div>{status}</div>
          <div className="label">Hiring Date</div>
          <div>{hiringDate}</div>
        </div>
        <div className="employee-detail-list">
          <div className="label">Email</div>
          <div>{email}</div>
          <div className="label">Phone Number</div>
          <div>{phoneNumber1}</div>
          <div className="label">Address</div>
          <div>{address}</div>
        </div>
      </div>
      <div className="employee-detail-toolbar">
        <InfoIcon className="employee-detail-toolbar-icon" onClick={handleClickOnDetails} />
        <EditIcon className="employee-detail-toolbar-icon" onClick={handleClickOnEdit} />
        <HideIcon className="employee-detail-toolbar-icon" onClick={handleShowDetails} />
      </div>
    </div>
  )
}

export default EmployeeDetail
