import { Button, Form, Input, Select } from 'antd'
import UploadAvatar from '../Upload/UploadAvatar'
import { toast } from 'react-hot-toast'
import { useState } from 'react'
import { UseMutateFunction } from '@tanstack/react-query'
import { useDepartments } from '@src/modules/settings/hooks/useDepartments'

const supabaseBucket = 'avatars'

interface IEmployeeFormProps {
  createEmployee?: UseMutateFunction<any[], Error, any, unknown>
  editEmployee?: UseMutateFunction<any, Error, any, unknown>
  userMode?: boolean
  action: 'add' | 'edit'
  employee?: {
    id: number
    created_at: Date
    fullName: string
    avatar: string
    email: string
    role: string
    gender: string
    birthday: string
    address: string
    phoneNumber1: string
    phoneNumber2: string
    job: string
    department: string
    status: string
    hiringDate: string
    leaveRate: number
  }
}

function EmployeeForm({
  createEmployee,
  editEmployee,
  action,
  employee,
  userMode,
}: IEmployeeFormProps) {
  const [avatar, setAvatar] = useState(employee?.avatar || '')
  const { departments } = useDepartments()

  const onAddEmployee = (values: any) => {
    const employee = { ...values.user, role: 'employee', avatar }

    if (!avatar) {
      toast.error('No Avatar is provided')
      return
    }
    if (createEmployee) createEmployee(employee)
  }

  const onEditEmployee = (values: any) => {
    if (!avatar) {
      toast.error('No Avatar is provided')
      return
    }
    const newEmployeeData = { ...values.user, avatar }

    if (editEmployee) editEmployee({ id: employee?.id, newEmployeeData })
  }

  return (
    <Form
      className="form-employee"
      layout="vertical"
      onFinish={action === 'add' ? onAddEmployee : onEditEmployee}
      style={{ width: 'inherit' }}
    >
      <div>
        <h3 className="form-section-header">Profil Photo</h3>
        <UploadAvatar avatar={avatar} setAvatar={setAvatar} supabaseBucket={supabaseBucket} />
      </div>
      <div>
        <h3 className="form-section-header">Personal Info</h3>
        <div className="form-section">
          <Form.Item
            name={['user', 'fullName']}
            label="Full Name"
            rules={[{ required: true }]}
            style={{ width: '280px' }}
            initialValue={employee?.fullName}
          >
            <Input style={{ height: '44px' }} />
          </Form.Item>

          <Form.Item
            name={['user', 'email']}
            label="Email"
            rules={[{ type: 'email', required: true }]}
            style={{ width: '280px' }}
            initialValue={employee?.email}
          >
            <Input style={{ height: '44px' }} disabled={true} />
          </Form.Item>

          <Form.Item
            name={['user', 'gender']}
            label="Gender"
            rules={[{ required: true }]}
            style={{ width: '280px' }}
            initialValue={employee?.gender}
          >
            <Select style={{ height: '44px' }}>
              <Select.Option value="male">Male</Select.Option>
              <Select.Option value="female">Female</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            name={['user', 'birthday']}
            label="Birthday"
            rules={[{ required: true }]}
            style={{ width: '280px' }}
            initialValue={employee?.birthday}
          >
            <Input style={{ height: '44px' }} placeholder="DD.MM.YYYY" />
          </Form.Item>

          <Form.Item
            name={['user', 'address']}
            label="Address"
            rules={[{ required: true }]}
            style={{ width: '280px' }}
            initialValue={employee?.address}
          >
            <Input style={{ height: '44px' }} />
          </Form.Item>

          <Form.Item
            name={['user', 'phoneNumber1']}
            label="Phone Number 1"
            rules={[{ required: true }]}
            style={{ width: '280px' }}
            initialValue={employee?.phoneNumber1}
          >
            <Input style={{ height: '44px' }} placeholder="+216 20 000 000" />
          </Form.Item>

          <Form.Item
            name={['user', 'phoneNumber2']}
            label="Phone Number 2 (Optional)"
            style={{ width: '280px' }}
            initialValue={employee?.phoneNumber2}
          >
            <Input style={{ height: '44px' }} placeholder="+216 20 000 000" />
          </Form.Item>
        </div>
      </div>
      <div>
        <h3 className="form-section-header">Professional Info</h3>
        <div className="form-section">
          <Form.Item
            name={['user', 'job']}
            label="Job Title"
            rules={[{ required: true }]}
            style={{ width: '280px' }}
            initialValue={employee?.job}
          >
            <Input
              style={{ height: '44px' }}
              placeholder="Ex: Front-End developer"
              disabled={userMode || false}
            />
          </Form.Item>

          <Form.Item
            name={['user', 'department']}
            label="Department"
            rules={[{ required: true }]}
            style={{ width: '280px' }}
            initialValue={employee?.department}
          >
            <Select style={{ height: '44px' }} disabled={userMode || false}>
              {departments?.map((department) => (
                <Select.Option key={department?.id} value={department?.name}>
                  {department?.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            name={['user', 'status']}
            label="Status"
            rules={[{ required: true }]}
            style={{ width: '280px' }}
            initialValue={employee?.status}
          >
            <Select style={{ height: '44px' }} disabled={userMode || false}>
              <Select.Option value="Full Time">Full Time</Select.Option>
              <Select.Option value="Part Time">Part Time</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            name={['user', 'hiringDate']}
            label="Hiring Date"
            rules={[{ required: true }]}
            style={{ width: '280px' }}
            initialValue={employee?.hiringDate}
          >
            <Input
              style={{ height: '44px' }}
              placeholder="DD.MM.YYYY"
              disabled={userMode || false}
            />
          </Form.Item>

          <Form.Item
            name={['user', 'leaveRate']}
            label="Leave Rate per month"
            rules={[{ required: true }]}
            style={{ width: '280px' }}
            initialValue={employee?.leaveRate}
          >
            <Input style={{ height: '44px' }} disabled={userMode || false} />
          </Form.Item>
        </div>
      </div>
      <Form.Item wrapperCol={{ offset: 20 }}>
        <Button type="primary" htmlType="submit">
          {action === 'add' ? 'Add new Employee' : 'Edit employee'}
        </Button>
      </Form.Item>
    </Form>
  )
}

export default EmployeeForm
