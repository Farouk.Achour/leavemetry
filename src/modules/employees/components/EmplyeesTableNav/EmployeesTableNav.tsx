function EmployeesTableNav() {
  return (
    <>
      <div className="employees-table-nav-el employees-table-el">ID</div>
      <div className="employees-table-nav-el employees-table-el">Name</div>
      <div className="employees-table-nav-el employees-table-el">Department</div>
      <div className="employees-table-nav-el employees-table-el">Status</div>
      <div className="employees-table-nav-el employees-table-el">Contacts</div>
      <div className="employees-table-nav-el employees-table-el"></div>
    </>
  )
}

export default EmployeesTableNav
