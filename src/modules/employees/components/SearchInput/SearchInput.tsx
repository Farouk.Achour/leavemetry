import { ReactComponent as SearchIcon } from '../../../shared/assets/icons/employees/search.svg'

interface ISearchInputProps {
  searchedEmployee: string
  setSearchedEmployee: any
  children: string
}

function SearchInput({ searchedEmployee, setSearchedEmployee, children }: ISearchInputProps) {
  return (
    <div className="input-wrapper">
      <input
        className="search-input"
        type="text"
        placeholder={children}
        value={searchedEmployee}
        onChange={(e) => setSearchedEmployee(e.target.value)}
      />
      <div className="search-icon">
        <SearchIcon />
      </div>
    </div>
  )
}

export default SearchInput
