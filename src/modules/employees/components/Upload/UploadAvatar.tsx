import { Dispatch, SetStateAction, useState } from 'react'
import { Upload, UploadFile, UploadProps } from 'antd'

import supabase, { supabaseUrl } from '@src/modules/shared/services/supabase'
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons'

interface IUploadAvatarProps {
  avatar: string
  setAvatar: Dispatch<SetStateAction<string>>
  supabaseBucket: string
}

function UploadAvatar({ avatar, setAvatar, supabaseBucket }: IUploadAvatarProps) {
  const [loading, setLoading] = useState(false)
  const [fileList, setFileList] = useState<UploadFile[]>([])

  const customRequest: UploadProps['customRequest'] = async ({ file, onSuccess, onError }) => {
    try {
      setLoading(true)

      const uploadFile = file as any

      const { data, error } = await supabase.storage
        .from(supabaseBucket)
        .upload(`public/${uploadFile.name}`, uploadFile, { upsert: true })

      if (error) {
        throw new Error()
      }

      const publicUrl =
        supabaseUrl + '/storage/v1/object/public/' + `${supabaseBucket}/` + data.path

      setAvatar(publicUrl)

      onSuccess?.('File uploaded successfully!')
      setLoading(false)
    } catch (error) {
      onError?.({ message: 'File upload failed.', name: 'Upload Error' })
      setLoading(false)
    }
  }

  const onChange: UploadProps['onChange'] = ({ fileList }) => {
    setFileList(fileList)
  }

  const onRemove: UploadProps['onRemove'] = (file) => {
    const newFileList = fileList.filter((item) => item.uid !== file.uid)
    setFileList(newFileList)
  }

  const uploadProps: UploadProps = {
    customRequest,
    fileList: [],
    onChange,
    onRemove,
  }

  const uploadButton = (
    <button
      style={{
        border: 0,
        background: '#DBDBDB',
        width: '94px',
        height: '94px',
        color: '#8e8e8e',
        borderRadius: '100%',
        cursor: 'pointer',
      }}
      type="button"
    >
      {loading ? <LoadingOutlined /> : <PlusOutlined style={{ transform: 'scale(1.5)' }} />}
    </button>
  )

  return (
    <Upload {...uploadProps}>
      {loading ? (
        uploadButton
      ) : avatar ? (
        <img
          src={avatar}
          alt="avatar"
          style={{ width: '94px', height: '94px', borderRadius: '100%', cursor: 'pointer' }}
        />
      ) : (
        uploadButton
      )}
    </Upload>
  )
}

export default UploadAvatar
