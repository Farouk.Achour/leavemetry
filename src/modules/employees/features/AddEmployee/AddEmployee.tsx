import { useCreateEmployee } from '../../hooks/useCreateEmployee'
import EmployeeForm from '../../components/EmployeeForm/EmployeeForm'

function AddEmployee() {
  const { createEmployee } = useCreateEmployee()

  return <EmployeeForm createEmployee={createEmployee} action="add" />
}

export default AddEmployee
