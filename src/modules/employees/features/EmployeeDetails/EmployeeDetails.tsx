import { useNavigate, useSearchParams } from 'react-router-dom'
import { useEmployeeById } from '../../hooks/useEmployeeById'
import { Button } from 'antd'
import { useDeleteEmployee } from '../../hooks/useDeleteEmployee'
import { useDeleteUser } from '@src/modules/auth/hook/useDeleteUser'
import useNavigateParams from '@src/modules/shared/hooks/useNavigateParams'
import { useMyLeaves } from '@src/modules/space/hooks/useMyLeaves'
import { calculateLeaveBalance } from '@src/modules/shared/utils/calculateLeaveBalance'

function EmployeeDetails() {
  const { deleteEmployee } = useDeleteEmployee()
  const { deleteUser } = useDeleteUser()
  const navigate = useNavigate()

  const navigateParams = useNavigateParams()

  const [searchParams] = useSearchParams()
  const id = searchParams.get('id')

  const { employee, isLoading } = useEmployeeById(id || '')

  const leaves = useMyLeaves(employee?.[0]?.id || '0')?.leaves
  const acceptedLeaves = leaves?.filter((leave) => leave.status.includes('approved'))

  const leaveBalance = calculateLeaveBalance(employee?.[0], acceptedLeaves)

  if (isLoading) return null

  const {
    avatar,
    fullName,
    birthday,
    gender,
    job,
    department,
    status,
    hiringDate,
    email,
    phoneNumber1,
    address,
  } = employee?.[0]

  function handleDeleteUser() {
    if (id) {
      deleteEmployee(+id)
      deleteUser(id, { onSettled: () => navigate(-1) })
    }
  }

  function handleEditUser() {
    navigateParams('/employees/edit', {
      id: id + '',
    })
  }

  return (
    <div className="employee-details-layout">
      <div>
        <h1>Employee Details</h1>
        <div className="employee-card">
          <div className="employee-card-header">
            <div className="employee-card-header-info">
              {avatar ? (
                <img className="avatar" src={avatar} />
              ) : (
                <div className="avatar avatar-letter">{fullName.slice(0, 1)}</div>
              )}

              <div>
                <h1 className="employee-fullname">{fullName}</h1>
                <p>{job}</p>
              </div>
            </div>

            <div className="employee-card-header-btns">
              <Button onClick={handleDeleteUser} danger>
                Delete
              </Button>
              <Button onClick={handleEditUser}>Edit Employee</Button>
            </div>
          </div>
          <span>Personal Info</span>
          <div className="employee-card-list">
            <div className="label">ID</div>
            <div>{id}</div>
            <div className="label">Full Name</div>
            <div>{fullName}</div>
            <div className="label">Email Address</div>
            <div>{email}</div>
            <div className="label">Phone Number</div>
            <div>{phoneNumber1}</div>
            <div className="label">Birthday</div>
            <div>{birthday}</div>
            <div className="label">Gender</div>
            <div>{gender}</div>
            <div className="label">Address</div>
            <div>{address}</div>
          </div>
          <span>Professional Info</span>
          <div className="employee-card-list">
            <div className="label">Hiring Date</div>
            <div>{hiringDate}</div>
            <div className="label">Job Title</div>
            <div>{job}</div>
            <div className="label">Department</div>
            <div>{department}</div>
            <div className="label">Status</div>
            <div>{status}</div>
            <div className="label">Leave Balance</div>
            <div>{leaveBalance}</div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default EmployeeDetails
