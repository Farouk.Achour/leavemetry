import Button from '@src/modules/shared/components/Button/Button'
import SearchInput from '../../components/SearchInput/SearchInput'
import { Dispatch, SetStateAction } from 'react'
import { useNavigate } from 'react-router-dom'

interface IEmployeesHeaderProps {
  searchedEmployee: string
  setSearchedEmployee: Dispatch<SetStateAction<string>>
}

function EmployeesHeader({ searchedEmployee, setSearchedEmployee }: IEmployeesHeaderProps) {
  const navigate = useNavigate()

  function switchAddEmployeeRoute() {
    navigate('/employees/add')
  }

  return (
    <div className="employees-header">
      <SearchInput searchedEmployee={searchedEmployee} setSearchedEmployee={setSearchedEmployee}>
        Search for an employee
      </SearchInput>
      <Button onClick={switchAddEmployeeRoute}>＋ Add an employee</Button>
    </div>
  )
}

export default EmployeesHeader
