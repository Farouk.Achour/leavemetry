import { useSearchParams } from 'react-router-dom'
import EmployeeForm from '../../components/EmployeeForm/EmployeeForm'
import { useEmployeeById } from '../../hooks/useEmployeeById'
import { useEditEmployee } from '../../hooks/useEditEmployee'

function UpdateEmployee() {
  const { editEmployee } = useEditEmployee()

  const [searchParams] = useSearchParams()
  const id = searchParams.get('id')

  const { employee, isLoading } = useEmployeeById(id || '')

  if (isLoading) return null

  return <EmployeeForm action="edit" employee={employee?.[0]} editEmployee={editEmployee} />
}

export default UpdateEmployee
