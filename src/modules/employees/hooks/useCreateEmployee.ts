import { useMutation } from '@tanstack/react-query'
import { createEmployee as createEmployeeApi } from '@src/modules/shared/services/apiEmployees'
import { toast } from 'react-hot-toast'
import { useNavigate } from 'react-router-dom'

export function useCreateEmployee() {
  const navigate = useNavigate()

  const { mutate: createEmployee, isSuccess } = useMutation({
    mutationFn: (args: any) => createEmployeeApi(args),
    onSuccess: () => {
      toast.success('User is created')
      navigate('/employees')
    },
    onError: (err) => {
      toast.error(err.message)
    },
  })

  return { createEmployee, isSuccess }
}
