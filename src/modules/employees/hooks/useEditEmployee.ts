import { updateEmployee } from '@src/modules/shared/services/apiEmployees'
import { useMutation } from '@tanstack/react-query'
import { toast } from 'react-hot-toast'
import { useNavigate } from 'react-router-dom'

export function useEditEmployee() {
  const navigate = useNavigate()

  const { mutate: editEmployee } = useMutation({
    mutationFn: ({ id, newEmployeeData }: any) => {
      return updateEmployee(id, newEmployeeData)
    },
    onSuccess: () => {
      toast.success('Employee successfully edited')
      navigate('/employees')
    },
    onError: (err) => toast.error(err.message),
  })

  return { editEmployee }
}
