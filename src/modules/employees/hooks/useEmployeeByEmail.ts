import { useQuery } from '@tanstack/react-query'
import { getOneEmployeeByEmail } from '../../shared/services/apiEmployees'

export function useEmployeeByEmail(email: string) {
  const { isLoading, data: employee } = useQuery({
    queryKey: ['employee', email],
    queryFn: () => getOneEmployeeByEmail(email),
  })

  return { isLoading, employee }
}
