import { useQuery } from '@tanstack/react-query'
import { getOneEmployeeById } from '../../shared/services/apiEmployees'

export function useEmployeeById(id: string) {
  const { isLoading, data: employee } = useQuery({
    queryKey: ['employee', id],
    queryFn: () => getOneEmployeeById(id),
  })

  return { isLoading, employee }
}
