import { useQuery } from '@tanstack/react-query'
import { getEmployees } from '../../shared/services/apiEmployees'

export function useEmployees() {
  const { isLoading, data: employees } = useQuery({
    queryKey: ['employees'],
    queryFn: getEmployees,
  })

  return { isLoading, employees }
}
