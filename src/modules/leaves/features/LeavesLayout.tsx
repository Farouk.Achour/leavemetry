import { Tabs, TabsProps } from 'antd'
import { useLeaves } from '../hooks/useLeaves'
import LeavesTable from '@src/modules/space/components/LeavesTable/LeavesTable'

const LeavesLayout = () => {
  const { leaves, isLoading } = useLeaves()
  if (isLoading) return null
  const waitingForTechnicalApprovalLeaves = leaves?.filter((leave) => leave.status.includes('tech'))
  const waitingForHRApprovalLeaves = leaves?.filter((leave) => leave.status.includes('hr'))
  const approvedLeaves = leaves?.filter((leave) => leave.status.includes('approved'))
  const rejectedLeaves = leaves?.filter((leave) => leave.status.includes('rejected'))
  const negotiationLeaves = leaves?.filter((leave) => leave.status.includes('negotiation'))

  const items: TabsProps['items'] = [
    {
      key: '1',
      label: 'Waiting for technique approval',
      children: <LeavesTable leaves={waitingForTechnicalApprovalLeaves} />,
    },
    {
      key: '2',
      label: 'Waiting for HR approval',
      children: <LeavesTable leaves={waitingForHRApprovalLeaves} hrControl={true} />,
    },
    {
      key: '3',
      label: 'Approved',
      children: <LeavesTable leaves={approvedLeaves} />,
    },
    {
      key: '4',
      label: 'Rejected',
      children: <LeavesTable leaves={rejectedLeaves} />,
    },
    {
      key: '5',
      label: 'Negotiation',
      children: <LeavesTable leaves={negotiationLeaves} />,
    },
  ]

  return <Tabs defaultActiveKey="1" items={items} />
}

export default LeavesLayout
