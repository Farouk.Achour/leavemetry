import UploadAvatar from '@src/modules/employees/components/Upload/UploadAvatar'
import { UseMutateFunction } from '@tanstack/react-query'
import { Button, Form, Input } from 'antd'
import TextArea from 'antd/es/input/TextArea'
import { useState } from 'react'
import { toast } from 'react-hot-toast'
import { IDepartment } from '../DepartmentCard/DepartmentCard'

const supabaseBucket = 'departments'

interface IDepartmentFormProps {
  createDepartment?: UseMutateFunction<any[], Error, any, unknown>
  editDepartment?: UseMutateFunction<any[], Error, any, unknown>
  action: 'add' | 'edit'
  department?: IDepartment
}

function DepartmentForm({
  action,
  department,
  createDepartment,
  editDepartment,
}: IDepartmentFormProps) {
  const [avatar, setAvatar] = useState(department?.photo || '')

  const onAddDepartment = (values: { department: IDepartment }) => {
    const departmentData = { ...values.department, photo: avatar }
    if (!avatar) {
      toast.error('No Avatar is provided')
      return
    }
    if (createDepartment) createDepartment(departmentData)
  }

  const onEditDepartment = (values: { department: IDepartment }) => {
    if (!avatar) {
      toast.error('No Avatar is provided')
      return
    }
    const newDepartmentData = { ...values.department, photo: avatar }
    if (editDepartment) editDepartment({ id: department?.id, newDepartmentData })
  }

  return (
    <Form
      className="form-department"
      layout="vertical"
      onFinish={action === 'add' ? onAddDepartment : onEditDepartment}
      style={{ width: 'inherit' }}
    >
      <div>
        <h1 className="form-department-headers">Department Photo</h1>
        <UploadAvatar avatar={avatar} setAvatar={setAvatar} supabaseBucket={supabaseBucket} />
      </div>
      <div>
        <h1 className="form-department-headers">Department Info</h1>
        <Form.Item
          name={['department', 'name']}
          label="Department Name"
          rules={[{ required: true }]}
          style={{ width: '280px' }}
          initialValue={department?.name}
        >
          <Input style={{ height: '44px' }} />
        </Form.Item>
        <Form.Item
          name={['department', 'chief']}
          label="Chief Department Name"
          rules={[{ required: true }]}
          style={{ width: '280px' }}
          initialValue={department?.chief}
        >
          <Input style={{ height: '44px' }} />
        </Form.Item>
        <Form.Item
          name={['department', 'description']}
          label="Description"
          rules={[{ required: true }]}
          style={{ width: '560px' }}
          initialValue={department?.description}
        >
          <TextArea rows={4} />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 20 }}>
          <Button type="primary" htmlType="submit">
            {action === 'add' ? 'Add new Department' : 'Edit Department'}
          </Button>
        </Form.Item>
      </div>
    </Form>
  )
}

export default DepartmentForm
