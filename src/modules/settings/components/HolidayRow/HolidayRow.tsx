import { useState } from 'react'
import { ReactComponent as EditIcon } from '../../../shared/assets/icons/edit.svg'
import { ReactComponent as TrashIcon } from '../../../shared/assets/icons/trash.svg'
import AddHolidayForm from '../../features/AddHolidayForm/AddHolidayForm'
import { useEditHoliday } from '../../hooks/useEditHoliday'
import { useDeleteHoliday } from '../../hooks/useDeleteHoliday'

export interface IHoliday {
  id: number
  created_at: string
  name: string
  date: string
  duration: number
  type: string
}

function HolidayRow({ holiday }: { holiday: IHoliday }) {
  const [editMode, setEditMode] = useState(false)
  const { id, name, date, duration, type } = holiday
  const { editHoliday } = useEditHoliday()
  const { deleteHoliday } = useDeleteHoliday()

  function handleEditMode() {
    setEditMode(true)
  }

  function handleDeleteHoliday() {
    deleteHoliday(id)
  }

  return (
    <>
      {editMode ? (
        <AddHolidayForm
          action="edit"
          holiday={holiday}
          setEditMode={setEditMode}
          editHoliday={editHoliday}
        />
      ) : (
        <>
          <div className="holidays-table-el">{name}</div>
          <div className="holidays-table-el">{date}</div>
          <div className="holidays-table-el">{duration}</div>
          <div className="holidays-table-el">{type}</div>
          <div className="holidays-table-el icons-toolbar">
            <EditIcon className="icons-toolbar-item" onClick={handleEditMode} />{' '}
            <TrashIcon className="icons-toolbar-item" onClick={handleDeleteHoliday} />
          </div>
        </>
      )}
    </>
  )
}

export default HolidayRow
