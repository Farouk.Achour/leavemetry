import { Button } from 'antd'
import HolidaysTableNav from '../HolidaysTableNav/HolidaysTableNav'
import { useState } from 'react'
import AddHolidayForm from '../../features/AddHolidayForm/AddHolidayForm'
import { useCreateHoliday } from '../../hooks/useCreateHoliday'
import { useHolidays } from '../../hooks/useHolidays'
import HolidayRow from '../HolidayRow/HolidayRow'

function HolidaysTable() {
  const [showAddHoliday, setShowAddHoliday] = useState(false)

  const { createHoliday } = useCreateHoliday()

  const { holidays, isLoading } = useHolidays()
  if (isLoading) return null

  function handleShowAddHoliday() {
    setShowAddHoliday(true)
  }

  return (
    <>
      <div className="holidays-table">
        <HolidaysTableNav />

        {holidays?.map((holiday) => <HolidayRow holiday={holiday} key={holiday?.id} />)}

        {showAddHoliday && (
          <AddHolidayForm
            action="add"
            createHoliday={createHoliday}
            setShowAddHoliday={setShowAddHoliday}
          />
        )}
      </div>
      <Button type="text" block className="add-holiday-btn" onClick={handleShowAddHoliday}>
        ＋ Add a holiday in the system
      </Button>
    </>
  )
}

export default HolidaysTable
