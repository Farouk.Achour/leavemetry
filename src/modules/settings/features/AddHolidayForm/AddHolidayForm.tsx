import { Button, Form, Input, Select } from 'antd'
import { IHoliday } from '../../components/HolidayRow/HolidayRow'
import { UseMutateFunction } from '@tanstack/react-query'
import { Dispatch, SetStateAction } from 'react'

interface IAddHolidayFormProps {
  holiday?: IHoliday
  action: 'add' | 'edit'
  createHoliday?: UseMutateFunction<any, Error, any, unknown>
  editHoliday?: UseMutateFunction<any, Error, any, unknown>
  setShowAddHoliday?: Dispatch<SetStateAction<boolean>>
  setEditMode?: Dispatch<SetStateAction<boolean>>
}

function AddHolidayForm({
  holiday,
  action,
  createHoliday,
  editHoliday,
  setShowAddHoliday,
  setEditMode,
}: IAddHolidayFormProps) {
  function handleAddHoliday({ holiday }: { holiday: IHoliday }) {
    if (createHoliday) createHoliday(holiday)
    if (setShowAddHoliday) setShowAddHoliday(false)
  }

  function handleEditHoliday(values: { holiday: IHoliday }) {
    const newHolidayData = { ...values.holiday, id: holiday?.id }
    if (editHoliday) editHoliday({ id: holiday?.id, newHolidayData })
    if (setEditMode) setEditMode(false)
  }

  return (
    <Form
      layout="vertical"
      className="holiday-form"
      onFinish={action === 'add' ? handleAddHoliday : handleEditHoliday}
    >
      <Form.Item
        name={['holiday', 'name']}
        rules={[{ required: true }]}
        style={{ width: '240px' }}
        initialValue={holiday?.name}
      >
        <Input style={{ height: '44px' }} />
      </Form.Item>
      <Form.Item
        name={['holiday', 'date']}
        rules={[{ required: true }]}
        style={{ width: '240px' }}
        initialValue={holiday?.date}
      >
        <Input style={{ height: '44px' }} />
      </Form.Item>
      <Form.Item
        name={['holiday', 'duration']}
        rules={[{ required: true }]}
        style={{ width: '240px' }}
        initialValue={holiday?.duration}
      >
        <Input style={{ height: '44px' }} />
      </Form.Item>

      <Form.Item
        name={['holiday', 'type']}
        rules={[{ required: true }]}
        style={{ width: '240px' }}
        initialValue={holiday?.type}
      >
        <Select style={{ height: '44px' }}>
          <Select.Option value="paid">Paid</Select.Option>
          <Select.Option value="unpaid">Unpaid</Select.Option>
        </Select>
      </Form.Item>

      <Form.Item>
        <Button style={{ height: '44px', width: '180px' }} htmlType="submit">
          Save
        </Button>
      </Form.Item>
    </Form>
  )
}

export default AddHolidayForm
