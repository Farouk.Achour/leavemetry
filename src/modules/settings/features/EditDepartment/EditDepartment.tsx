import { useSearchParams } from 'react-router-dom'
import { useEditDepartment } from '../../hooks/useEditDepartment'
import { useDepartmentById } from '../../hooks/useDepartmentById'
import DepartmentForm from '../../components/DepartmentForm/DepartmentForm'

function EditDepartment() {
  const { editDepartment } = useEditDepartment()

  const [searchParams] = useSearchParams()
  const id = searchParams.get('id')

  const { department, isLoading } = useDepartmentById(id || '')

  if (isLoading) return null

  return (
    <DepartmentForm action="edit" department={department?.[0]} editDepartment={editDepartment} />
  )
}

export default EditDepartment
