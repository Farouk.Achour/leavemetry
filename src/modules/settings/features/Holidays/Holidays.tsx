import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import { useNavigate } from 'react-router-dom'
import { useUser } from '@src/modules/auth/hook/useUser'
import { useEmployeeByEmail } from '@src/modules/employees/hooks/useEmployeeByEmail'
import { useHolidays } from '../../hooks/useHolidays'
import { formatDate } from '@fullcalendar/core/index.js'

function Holidays() {
  const navigate = useNavigate()

  const { user } = useUser()
  const { employee, isLoading } = useEmployeeByEmail(user?.email || '')
  const { holidays, isLoading: isLoadingHolidays } = useHolidays()

  if (isLoading) return null
  if (isLoadingHolidays) return null

  const role = employee?.[0]?.role

  function handleAddHolidays() {
    navigate('/settings/holidays/add')
  }

  const events = holidays?.map((holiday) => {
    const endDate = new Date(holiday?.date)
    endDate.setDate(endDate.getDate() + holiday?.duration)
    let end: any = formatDate(endDate).split('/')
    const year = end[2]
    const month = end[0].length === 2 ? end[0] : '0' + end[0]
    const day = end[1].length === 2 ? end[1] : '0' + end[1]

    end = year + '-' + month + '-' + day

    const newHoliday = {
      start: holiday?.date,
      end,
      color: holiday?.type === 'paid' ? 'green' : 'red',
      display: 'background',
      title: holiday?.name,
    }
    return newHoliday
  })

  return (
    <>
      <FullCalendar
        plugins={[dayGridPlugin]}
        initialView="dayGridMonth"
        height={'80vh'}
        events={events || {}}
        customButtons={{
          myCustomButton: {
            text: 'Add a New Holiday',
            click: handleAddHolidays,
          },
        }}
        headerToolbar={{
          start: 'title',
          center: `${role === 'HR' ? 'myCustomButton' : ''}`,
        }}
      />
      <div className="calendar-footer">
        <div className="calendar-footer-filter">
          <div className="green-box"></div>
          <div>Non-working and paid public holidays.</div>
        </div>
        <div className="calendar-footer-filter">
          <div className="red-box"></div>
          <div>Non-working and unpaid public holidays.</div>
        </div>
        <div className="calendar-footer-filter">
          <div className="yellow-box"></div>
          <div>Today</div>
        </div>
      </div>
    </>
  )
}

export default Holidays
