import { useUser } from '@src/modules/auth/hook/useUser'
import EmployeeForm from '@src/modules/employees/components/EmployeeForm/EmployeeForm'
import { useEditEmployee } from '@src/modules/employees/hooks/useEditEmployee'
import { useEmployeeByEmail } from '@src/modules/employees/hooks/useEmployeeByEmail'

function UserSettings() {
  const { editEmployee } = useEditEmployee()

  const { user } = useUser()
  const { employee, isLoading } = useEmployeeByEmail(user?.email || '')

  if (isLoading) return null

  return (
    <EmployeeForm
      action="edit"
      userMode={true}
      editEmployee={editEmployee}
      employee={employee?.[0]}
    />
  )
}

export default UserSettings
