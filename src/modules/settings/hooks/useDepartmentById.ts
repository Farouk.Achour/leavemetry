import { getOneDepartmentById } from '@src/modules/shared/services/apiDepartments'
import { useQuery } from '@tanstack/react-query'

export function useDepartmentById(id: string) {
  const { isLoading, data: department } = useQuery({
    queryKey: ['department', id],
    queryFn: () => getOneDepartmentById(id),
  })

  return { isLoading, department }
}
