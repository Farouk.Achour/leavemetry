import { getDepartments } from '@src/modules/shared/services/apiDepartments'
import { useQuery } from '@tanstack/react-query'

export function useDepartments() {
  const { isLoading, data: departments } = useQuery({
    queryKey: ['departments'],
    queryFn: getDepartments,
  })

  return { isLoading, departments }
}
