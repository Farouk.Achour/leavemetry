import { updateHoliday } from '@src/modules/shared/services/apiHolidays'
import { useMutation, useQueryClient } from '@tanstack/react-query'
import { toast } from 'react-hot-toast'

export function useEditHoliday() {
  const queryClient = useQueryClient()

  const { mutate: editHoliday } = useMutation({
    mutationFn: ({ id, newHolidayData }: any) => {
      return updateHoliday(id, newHolidayData)
    },
    onSuccess: () => {
      toast.success('Holiday successfully edited')

      queryClient.invalidateQueries({
        queryKey: ['holidays'],
      })
    },
    onError: (err) => toast.error(err.message),
  })

  return { editHoliday }
}
