/* eslint-disable @typescript-eslint/no-explicit-any */
import MainLayout from '@src/modules/shared/layout/MainLayout/MainLayout'
import AuthGuard from '@src/modules/shared/guards/AuthGuard'
import { RouteProps } from 'react-router-dom'
import { Fragment, lazy } from 'react'

type RouteConfig = {
  exact: boolean | null
  path: string
  component: React.ComponentType<any>
  guard?: React.ComponentType<any> | typeof Fragment | any
  layout?: React.ComponentType<any> | typeof Fragment
} & RouteProps

const routes: RouteConfig[] = [
  // AuthGuard Routes
  {
    exact: true,
    guard: AuthGuard,
    path: '/settings/department',
    component: lazy(() => import('../features/Department/Department')),
    layout: MainLayout,
  },
  {
    exact: true,
    guard: AuthGuard,
    path: '/settings/department/add',
    component: lazy(() => import('../features/AddDepartment/AddDepartment')),
    layout: MainLayout,
  },
  {
    exact: true,
    guard: AuthGuard,
    path: '/settings/department/edit',
    component: lazy(() => import('../features/EditDepartment/EditDepartment')),
    layout: MainLayout,
  },
  {
    exact: true,
    guard: AuthGuard,
    path: '/settings/user',
    component: lazy(() => import('../features/UserSettings/UserSettings')),
    layout: MainLayout,
  },
  {
    exact: true,
    guard: AuthGuard,
    path: '/settings/holidays',
    component: lazy(() => import('../features/Holidays/Holidays')),
    layout: MainLayout,
  },
  {
    exact: true,
    guard: AuthGuard,
    path: '/settings/holidays/add',
    component: lazy(() => import('../features/AddHoliday/AddHoliday')),
    layout: MainLayout,
  },
]

export default routes
