import { useLocation, useNavigate } from 'react-router-dom'
import { ReactComponent as ProfileIcon } from '../../assets/icons/sidebar/profile.svg'
import { ReactComponent as LogoutIcon } from '../../assets/icons/navbar/logout.svg'
import { useAppDispatch } from '../../store'
import { logout } from '@src/modules/auth/data/authThunk'
import { useState } from 'react'
import Dropdown from '../DropDown/DropDown'
import CustomAvatar from '../Avatar/Avatar'
import { useUser } from '@src/modules/auth/hook/useUser'

const Navbar: React.FC = () => {
  const { pathname } = useLocation()
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const [isSettingOpen, setIsSettingOpen] = useState(false)

  const handleLogout = () => {
    dispatch(logout())
  }

  const { user } = useUser()

  const accountInfoItems = [
    {
      key: '1',
      label: (
        <div className="user-info-container">
          <CustomAvatar image={null} text={user?.email || 'User'} size={40} />

          <div className="navbar-account-info">
            <p className="sidebar-accountinfo-item">{user?.email}</p>
          </div>
        </div>
      ),
      disabled: true,
      onClick: () => navigate('/settings/user'),
    },
    {
      key: '2',
      label: <p>Profile</p>,
      icon: <ProfileIcon style={{ stroke: 'black', width: '18px', height: '18px' }} />,
      onClick: () => navigate('/settings/user'),
    },
    {
      key: '4',
      label: <p>logout</p>,
      icon: <LogoutIcon style={{ stroke: 'black', width: '18px', height: '18px' }} />,
      onClick: handleLogout,
    },
  ]

  const pathDetails = pathname.split('/')[2]
  let navPathDetail = ''
  if (pathDetails === 'add') navPathDetail = '/ Add New Employee'
  if (pathDetails === 'edit') navPathDetail = '/ Edit Employee'

  return (
    <div className="navbar">
      <div className="navbar-left">
        <p className="navbar-left-title">
          {pathname.split('/')[2] ? pathname.split('/')[2] : pathname.split('/')[1]} {navPathDetail}
        </p>
      </div>
      <div className="navbar-right">
        <Dropdown
          isOpen={isSettingOpen}
          setIsOpen={setIsSettingOpen}
          items={accountInfoItems}
          triggerElement={
            <button onClick={() => setIsSettingOpen(true)} className="navbar-avatar-btn">
              <CustomAvatar image={null} text="User" size={40} />
            </button>
          }
        ></Dropdown>
      </div>
    </div>
  )
}

export default Navbar
