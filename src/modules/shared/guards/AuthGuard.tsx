import { Navigate } from 'react-router-dom'
import { useAppSelector } from '../store'
import { useEmployeeByEmail } from '@src/modules/employees/hooks/useEmployeeByEmail'

interface MainLayoutProps {
  children: React.ReactNode
}

const AuthGuard = ({ children }: MainLayoutProps) => {
  const { user } = useAppSelector((state) => state?.auth)

  const { employee } = useEmployeeByEmail(user?.email || '')

  const role = employee?.[0]?.role

  const path = window.location.pathname

  const isAuthenticated = useAppSelector((state) => state.auth.isAuthenticated)

  if (
    role === 'employee' &&
    (path.includes('employees') || path.includes('leaves') || path.includes('department'))
  )
    return <Navigate to="/dashboard" />

  return isAuthenticated ? children : <Navigate to="/login" />
}

export default AuthGuard
