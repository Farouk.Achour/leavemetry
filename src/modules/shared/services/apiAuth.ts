import supabase from './supabase'

export async function getCurrentUser() {
  const { data: session } = await supabase.auth.getSession()
  if (!session.session) return null

  const { data, error } = await supabase.auth.getUser()

  if (error) throw new Error(error.message)
  return data?.user
}

export async function deleteUser(id: string) {
  const { data, error } = await supabase.auth.admin.deleteUser(id)

  if (error) return

  return data
}
