import supabase from './supabase'

export async function getDepartments() {
  let { data: departments, error } = await supabase.from('departments').select('*')

  if (error) return

  return departments
}

export async function getOneDepartmentById(id: string) {
  let { data: department, error } = await supabase.from('departments').select('*').eq('id', id)

  if (error) return

  return department
}

export async function createDepartment(department: any) {
  const { data, error } = await supabase.from('departments').insert(department).select()

  if (error) throw new Error(error.message)

  return data
}

export async function updateDepartment(id: number, obj: any) {
  const { data, error } = await supabase
    .from('departments')
    .update(obj)
    .eq('id', id)
    .select()
    .single()

  if (error) {
    throw new Error('Department could not be updated')
  }

  return data
}

export async function deleteDepartment(id: number) {
  const { data, error } = await supabase.from('departments').delete().eq('id', id)

  if (error) throw new Error('Department could not be deleted')

  return data
}
