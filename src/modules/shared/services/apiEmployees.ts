import supabase from '@src/modules/shared/services/supabase'

export async function getEmployees() {
  let { data: employees, error } = await supabase.from('employees').select('*')

  if (error) return

  return employees
}

export async function getOneEmployeeByEmail(email: string) {
  let { data: employee, error } = await supabase.from('employees').select('*').eq('email', email)

  if (error) return

  return employee
}

export async function getOneEmployeeById(id: string) {
  let { data: employee, error } = await supabase.from('employees').select('*').eq('id', id)

  if (error) return

  return employee
}

export async function createEmployee(employee: any) {
  const { data, error } = await supabase.from('employees').insert(employee).select()

  if (error) throw new Error(error.message)

  const { error: inviteError } = await supabase.auth.admin.inviteUserByEmail(employee?.email, {
    redirectTo: `http://localhost:3000/auth/reset-password?email=${data[0]?.email}`,
  })
  if (inviteError) throw new Error(inviteError.message)

  return data
}

export async function updateEmployee(id: number, obj: any) {
  const { data, error } = await supabase
    .from('employees')
    .update(obj)
    .eq('id', id)
    .select()
    .single()

  if (error) {
    console.error(error)
    throw new Error('Employee could not be updated')
  }
  return data
}

export async function deleteEmployee(id: number) {
  const employee = await getOneEmployeeById(id + '')

  const userEmail = employee?.[0]?.email

  const {
    data: { users },
  } = await supabase.auth.admin.listUsers()

  const userId = users.filter((user) => user.email === userEmail)?.[0]?.id

  await supabase.auth.admin.deleteUser(userId + '')

  const { data, error } = await supabase.from('employees').delete().eq('id', id)

  if (error) {
    console.error(error)
    throw new Error('Employee could not be deleted')
  }
  return data
}
