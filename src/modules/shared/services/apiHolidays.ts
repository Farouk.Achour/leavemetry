import supabase from './supabase'

export async function getHolidays() {
  let { data: holidays, error } = await supabase.from('holidays').select('*')

  if (error) return

  return holidays
}

export async function createHoliday(holiday: any) {
  const { data, error } = await supabase.from('holidays').insert(holiday).select()

  if (error) throw new Error(error.message)

  return data
}

export async function updateHoliday(id: number, obj: any) {
  const { data, error } = await supabase.from('holidays').update(obj).eq('id', id).select().single()

  if (error) throw new Error('Holiday could not be updated')

  return data
}

export async function deleteHoliday(id: number) {
  const { data, error } = await supabase.from('holidays').delete().eq('id', id)

  if (error) throw new Error('Holiday could not be deleted')

  return data
}
