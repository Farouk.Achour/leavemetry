import supabase from './supabase'

export async function getAllLeaves() {
  let { data: leaves, error } = await supabase.from('leaves').select('*').order('id')

  if (error) return

  return leaves
}

export async function getMyLeaves(id: string) {
  let { data: leaves, error } = await supabase.from('leaves').select('*').eq('employeeId', id)

  if (error) return

  return leaves
}

export async function createLeave(leave: any) {
  const { data, error } = await supabase.from('leaves').insert(leave).select()

  if (error) throw new Error(error.message)

  return data
}

export async function updateLeave(id: number, obj: any) {
  const { data, error } = await supabase.from('leaves').update(obj).eq('id', id).select().single()

  if (error) {
    console.error(error)
    throw new Error('Leave could not be updated')
  }
  return data
}
