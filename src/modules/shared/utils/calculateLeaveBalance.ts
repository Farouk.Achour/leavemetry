import { ILeave } from '@src/modules/space/components/LeavesTable/LeavesTable'

export function calculateLeaveBalance(employee: any, myAcceptedLeaves: any) {
  let leaveRate = employee?.leaveRate
  leaveRate = Math.ceil((leaveRate * 100) / 30) / 100

  let hiringDate = employee?.hiringDate?.includes('.')
    ? employee?.hiringDate.split('.')
    : employee?.hiringDate.split('/')
  if (!hiringDate) return null

  const year = hiringDate[2]
  const month = hiringDate[0].length === 2 ? hiringDate[0] : '0' + hiringDate[0]
  const day = hiringDate[1].length === 2 ? hiringDate[1] : '0' + hiringDate[1]

  hiringDate = year + '-' + day + '-' + month

  const numberOfDaysSinceHiring = Math.floor(
    (Date.now() - new Date(hiringDate).getTime()) / (1000 * 60 * 60 * 24)
  )

  const daysOnHoliday = myAcceptedLeaves?.reduce((acc: number, cur: ILeave) => {
    const from = new Date(cur.from).getTime()
    const to = new Date(cur.to).getTime()

    const numberOfDaysInThisLeave = (to - from) / (1000 * 60 * 60 * 24) + 1
    return acc + numberOfDaysInThisLeave
  }, 0)

  const leaveBalance = Math.floor(numberOfDaysSinceHiring * leaveRate) - daysOnHoliday

  return leaveBalance
}
