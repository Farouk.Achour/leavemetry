import LeaveRow from '../LeaveRow/LeaveRow'
import LeavesTableNav from '../LeavesTableNav/LeavesTableNav'

export interface ILeave {
  id?: number
  created_at?: string
  type: string
  description: string
  from: string
  to: string
  employeeId?: number
  status?: string
}

interface ILeavesTableProps {
  leaves: ILeave[] | undefined
  hrControl?: boolean | undefined
}

function LeavesTable({ leaves, hrControl }: ILeavesTableProps) {
  return (
    <div className="leaves-table">
      <LeavesTableNav hrControl={hrControl} />
      {leaves?.map((leave: any) => <LeaveRow leave={leave} hrControl={hrControl} key={leave.id} />)}
    </div>
  )
}

export default LeavesTable
