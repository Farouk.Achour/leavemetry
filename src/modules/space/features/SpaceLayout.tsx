import { Tabs, TabsProps } from 'antd'
import { useMyLeaves } from '../hooks/useMyLeaves'
import { useUser } from '@src/modules/auth/hook/useUser'
import { useEmployeeByEmail } from '@src/modules/employees/hooks/useEmployeeByEmail'
import LeavesTable from '../components/LeavesTable/LeavesTable'

const SpaceLayout = () => {
  const { user } = useUser()
  const { employee } = useEmployeeByEmail(user?.email || '')

  const myLeaves = useMyLeaves(employee?.[0]?.id || '0')?.leaves
  const waitingForApprovalLeaves = myLeaves?.filter((leave) => leave.status.includes('pending'))
  const approvedLeaves = myLeaves?.filter((leave) => leave.status.includes('approved'))
  const rejectedLeaves = myLeaves?.filter((leave) => leave.status.includes('rejected'))
  const negotiationLeaves = myLeaves?.filter((leave) => leave.status.includes('negotiation'))

  const items: TabsProps['items'] = [
    {
      key: '1',
      label: 'Waiting for approval',
      children: <LeavesTable leaves={waitingForApprovalLeaves} />,
    },
    {
      key: '2',
      label: 'Approved',
      children: <LeavesTable leaves={approvedLeaves} />,
    },
    {
      key: '3',
      label: 'Rejected',
      children: <LeavesTable leaves={rejectedLeaves} />,
    },
    {
      key: '4',
      label: 'Negotiation',
      children: <LeavesTable leaves={negotiationLeaves} />,
    },
  ]

  return <Tabs defaultActiveKey="1" items={items} />
}

export default SpaceLayout
