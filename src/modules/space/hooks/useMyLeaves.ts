import { getMyLeaves } from '@src/modules/shared/services/apiLeaves'
import { useQuery } from '@tanstack/react-query'

export function useMyLeaves(id: string) {
  const { isLoading, data: leaves } = useQuery({
    queryKey: ['leaves', id],
    queryFn: () => getMyLeaves(id),
  })

  return { isLoading, leaves }
}
